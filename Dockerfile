FROM oraclelinux
RUN yum install -y httpd 
COPY Breakout /var/www/html
COPY  ./apache/mod_deflate.conf /etc/httpd/conf.d/mod_deflate.conf
ENTRYPOINT /usr/sbin/httpd -D FOREGROUND